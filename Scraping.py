import os
import sys
import openpyxl
from Professional import Professional


class Settings:
    def __init__(self):
        self.conditions = {}

        dpath = os.path.dirname(sys.argv[0])
        rpath = os.path.join(dpath, 'settings.xlsx')

        wb = openpyxl.load_workbook(rpath)
        sheet = wb['settings']
        for r in range(1, 100):
            key = sheet.cell(row=r, column=1).value
            if key is None:
                break
            values = []
            for c in range(2, 1000):
                value = sheet.cell(row=r, column=c).value
                if value is None:
                    break
                else:
                    values.append(value)

            self.conditions[key] = values


if __name__ == '__main__':
    settings = Settings()
    professional = Professional(settings.conditions)
    professional.search_areas()
