import os
import sys
import time
import datetime

import openpyxl
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from bs4 import BeautifulSoup
import pandas as pd
import re
import sqlite3
import warnings
warnings.simplefilter('ignore', FutureWarning)


class Professional:
    base_url = 'https://www.shares.ai/splist'
    current_dir = os.path.dirname(sys.argv[0])
    columns = ['士業事務所名', '住所', '士業種', '都道府県']

    def __init__(self, conditions):
        self.prefectures = conditions['都道府県']
        self.page_access_span = conditions['ページアクセス間隔'][0]
        self.max_page_size = conditions['最大ページサイズ'][0]
        self.rets = pd.DataFrame(index=[], columns=self.columns)
        self.ret_dir = os.path.join(self.current_dir, 'results')
        #        self.rets = pd.DataFrame(index=[], columns=self.columns)
        # ブラウザを立ち上げない
        options = Options()
        options.add_argument('--headless')
        options.add_argument("--no-sandbox")

        # 自身の環境にインストールされているGoogle Chromeのバージョンに併せてドライバーをインストール
        self.driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

    def search_areas(self):
        for prefecture in self.prefectures:
            self.rets = pd.DataFrame(index=[], columns=self.columns)
            self.search_prefecture(prefecture)
            fn = os.path.join(self.ret_dir, 'results_{}.xlsx'.format(prefecture))
            self.rets.to_excel(fn, index=False)

        self.driver.close()
        self.driver.quit()

    def search_prefecture(self, prefecture):
        self.driver.get(self.base_url)
        time.sleep(self.page_access_span)

        prefecture_link = self.driver.find_elements(By.CSS_SELECTOR, "a[href='/splist/{}/']".format(prefecture))
        if 0 == len(prefecture_link):
            return
        prefecture_link[0].click()
        time.sleep(self.page_access_span)

        self.search_pros(prefecture, 1)

    def search_pros(self, prefecture, view_page_cnt):
        print("都道府県：{}, ページ：{}".format(prefecture, view_page_cnt))

        html = self.driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, 'html.parser')

        table = soup.find('div', class_='main-content-box')
        professionals = table.findAll('div', class_='clearfix')
        for pro in professionals:
            company_name = self.getElementValue(pro.find('h2', class_='company-name'))
            address = self.getElementValue(pro.find('span', itemprop='addressLocality'))
            licenses_lst = pro.findAll('span', class_='license-name')
            licenses = ''
            for lic in licenses_lst:
                lic_txt = self.getElementValue(lic)
                if lic_txt == '中小企業診断士':
                    continue
                licenses = licenses + ',' + lic_txt if licenses != '' else lic_txt

            if licenses == '':
                continue

            self.rets = self.rets.append({
                '士業事務所名': company_name, '住所': address, '士業種': licenses, '都道府県': prefecture}
                , ignore_index=True)

        if view_page_cnt < self.max_page_size:
            next_page = self.driver.find_elements(By.CSS_SELECTOR, "a[href='{}']".format(view_page_cnt + 1))
            if 0 == len(next_page):
                return
            next_page[0].click()
            time.sleep(self.page_access_span)
            self.search_pros(prefecture, view_page_cnt + 1)

    @classmethod
    def getElementValue(cls, ele):
        return ele.text.strip() if ele is not None else ''
